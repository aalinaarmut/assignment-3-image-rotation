#ifndef BMP_HEADER_H
#define BMP_HEADER_H
#include "image.h"
#include <stdio.h>


struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
// статус для прочтения файла //
enum read_status {
 READ_OK = 0,

 READ_INVALID_BITS,
 READ_INVALID_HEADER,
 READ_SEGFAULT


};
// cтатус для записи в файл BMP //
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = 1

};

enum read_status from_bmp(FILE* in, struct image** source);
enum write_status to_bmp(FILE* out, struct image source);
#endif
