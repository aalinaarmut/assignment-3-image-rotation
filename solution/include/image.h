#ifndef image_h
#define image_h

#include <stdint.h>
struct pixel {
    uint8_t b, g, r;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};


struct image* create_image(uint64_t w, uint64_t h);

void free_image(struct image* source);

#endif
