#include "rotate.h"
#include "image.h"
#include <stddef.h>


struct image* rotate(struct image* source, int angle) {
    if (!source) {
        return NULL;
    }

    uint64_t w = source->width;
    uint64_t h = source->height;

    uint64_t transform_w, transform_h;


    switch (angle) {
        case 0:
        case 180:
        case -180:
            transform_w = w;
            transform_h = h;
            break;

        case 90:
        case -270:
        case -90:
        case 270:
            transform_h = w;
            transform_w = h;
            break;
        default:
            return NULL;
    }


    struct image *rotation = create_image(transform_w, transform_h);

    if (!rotation || !rotation->data) {
        free_image(rotation);
        return NULL;
    }

    for (uint64_t y = 0; y < h; ++y) {
        for (uint64_t x = 0; x < w; ++x) {
            uint64_t rotated_x, rotated_y;
            switch (angle) {
                case 180:
                case -180:
                    rotated_y = h - 1 - y;
                    rotated_x = w - 1 - x;
                    break;
                case 0:
                    rotated_y = y;
                    rotated_x = x;
                    break;
                case 90:
                case -270:
                    rotated_y = transform_h - 1 - x;
                    rotated_x = y;
                    break;
                case -90:
                case 270:
                    rotated_y = x;
                    rotated_x = transform_w - 1 - y;
                    break;
            }
            rotation->data[rotated_y * transform_w + rotated_x] = source->data[y * w + x];
        }
    }

    return rotation;

}
