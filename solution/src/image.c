#include "image.h"
#include <stdio.h>
#include <stdlib.h>



struct image* create_image(uint64_t w, uint64_t h) {
    struct image* source = (struct image*)malloc(sizeof(struct image));
    if (source == NULL) {
        perror("Ошибка связанная с памятью ");
        return NULL;
    }
    source->data = malloc(sizeof(struct pixel) * w * h);
    source->width = w;
    source->height = h;


    if (source->data == NULL) {
        free(source);
        perror("Ошибка связанная с памятью ");
        return NULL;
    }

    return source;
}




// если память была выделена освобождаем //
void free_image(struct image* source) {
    if (source != NULL) {
        free(source->data);
        free(source);
    } else {
        free(NULL);
    }
}

