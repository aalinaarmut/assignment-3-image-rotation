#include "bmp.h"
#include "rotate.h"

#include <stdio.h>
#include <stdlib.h>
#define ARGS 4
#define FAULT_EXIT 1


FILE* in(const char* filename, char* mode,  char* error_message) {
    FILE* open_file = fopen(filename, mode);
    if (open_file) {
        return open_file;
    
    } else {
        perror(error_message);
        return NULL;
    }
}

void rotateImageAndWrite(char* sourceImage, const char* new_file, int angle) {
    struct image* source;


    FILE* open_image = in(sourceImage, "rb", "Произошла ошибка при открытие файла.");
    if (open_image == NULL) {
        return;
    }


    enum read_status readingImage = from_bmp(open_image, &source);
    fclose(open_image);
    if (readingImage != READ_OK) {
        perror("Произошла ошибка при чтении изображения.");
        free_image(source);

    }

   
    struct image* rotating_image = rotate(source, angle);
    if (!rotating_image) {
        perror("Ошибка при создании изображения");
        free_image(source);
        return;
    }

    FILE* rotating_file_image = in(new_file, "wb", "Ошибка при открытие повернутого изображения");
    if (rotating_file_image == NULL) {
        free_image(source);
        free_image(rotating_image);
        return;
    }

    enum write_status writing_image = to_bmp(rotating_file_image,*rotating_image  );

    fclose(rotating_file_image);
    if (writing_image != WRITE_OK) {
        perror("Произошла ошибка при записи изображения");

    }


    free_image(source);
    free_image(rotating_image);
}


int main( int argc, char** argv ) {
   
    if (argc != ARGS){
        printf("Ошибка. Количество аргументов должно быть равно 4.");
        return FAULT_EXIT;
    }
    char* sourceImage = argv[1];
    char* new_file = argv[2];
   // int angle = (int) atoi(argv[3]);
    int angle = (int) strtol(*(argv + 3), (char **) '\0', 10);
    angle = angle % 360;
    if(angle % 90 != 0){
        fprintf(stderr, "Неправильно набран угол. Попробуйте еще раз");
        return -1;
    }

rotateImageAndWrite(sourceImage, new_file, angle);
return 0;
}
