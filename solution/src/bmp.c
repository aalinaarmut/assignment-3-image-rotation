#include "bmp.h"
#include <stdio.h>

#define BI_SIZE 40
#define BIT_COUNT 24
#define BI_PLANES 1
#define BMP_BITS sizeof(struct bmp_header)
#define RESERVED 0
#define PIXELS sizeof(struct pixel)
#define PADDING 4
#define TYPE 0x4D42
#define CONST_ZERO 0
#define CONST_ONE 1

static uint32_t padding_space(uint32_t width) {
    return (width == CONST_ZERO) ? CONST_ZERO : (PADDING - (width * PIXELS) % PADDING) % PADDING;
}

enum write_status writeHeader(FILE* out, const struct bmp_header* header) {
if (fwrite(header, sizeof(struct bmp_header), CONST_ONE, out) == CONST_ONE) {
        return WRITE_OK;
    } else {
        return WRITE_ERROR;
    }
}

enum read_status readHeader(FILE* in, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) == 1) {
        return READ_OK;
    } else {
        return READ_INVALID_HEADER;
    }
}

enum write_status to_bmp(FILE* out, const struct image source ) {
    struct bmp_header header;

    header.bfType = TYPE ;
    header.biWidth = source.width;
    uint32_t padding = padding_space(header.biWidth);
    header.bfileSize = BMP_BITS + (padding + source.width * PIXELS) * source.height;
    header.bfReserved = RESERVED;
    header.bOffBits = BMP_BITS;
    header.biSize = BI_SIZE;
    header.biHeight = source.height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BIT_COUNT;
    header.biCompression = CONST_ZERO;
    header.biSizeImage = CONST_ZERO;
    header.biXPelsPerMeter = CONST_ZERO;
    header.biYPelsPerMeter = CONST_ZERO;
    header.biClrUsed = CONST_ZERO;
    header.biClrImportant = CONST_ZERO;

    if (writeHeader(out, &header) == WRITE_OK) {
        for (uint32_t y = CONST_ZERO; y < source.height; ++y) {
            if (fwrite(&source.data[y * source.width], PIXELS, source.width, out) != source.width) {
                return WRITE_ERROR;
            }

            if (padding > CONST_ZERO && fseek(out, padding, SEEK_CUR) != CONST_ZERO) {
                return WRITE_ERROR;
            }
        }
        return WRITE_OK;
    }
    return WRITE_OK;
}

enum read_status from_bmp(FILE* in, struct image** source ) {
    struct bmp_header header;
    if (readHeader(in, &header) != READ_OK) {
        return READ_INVALID_HEADER;
    }
    *source = create_image(header.biWidth, header.biHeight);
    if (source == NULL) {
        return READ_SEGFAULT;
    }
    uint32_t padding = (PADDING - (header.biWidth * PIXELS % PADDING)) % PADDING;
    for (uint32_t j = CONST_ZERO; j < header.biHeight; ++j) {
        if (fread(&(*source)->data[j * header.biWidth], PIXELS, header.biWidth, in) != header.biWidth) {
            return READ_INVALID_BITS;
        }
        if (fseek(in, (long)padding, SEEK_CUR) != CONST_ZERO) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}
